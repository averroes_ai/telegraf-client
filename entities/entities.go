// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package entities

type Field struct {
	TomlKey string
	Value   interface{}
}

type Agent struct {
	Interval      string `toml:"interval"`
	FlushInterval string `toml:"flush_interval"`
	Logfile       string `toml:"logfile"`
}

type Tag struct {
	Key   string
	Value interface{}
}

type Object struct {
	Name   string
	Fields []Field
}

type Array struct {
	Name    string
	Fields  []Field
	Objects []Object
	Arrays  []Array
}

type IPlugin interface {
	GetType() string
	GetFields() []Field
	GetName() string
	GetTags() []Tag
	GetObjects() []Object
	GetArrays() []Array
}

type Plugin struct {
	Name    string
	Fields  []Field
	Tags    []Tag
	Objects []Object
	Arrays  []Array
}

func (p *Plugin) GetTags() []Tag {
	return p.Tags
}

func (p *Plugin) GetFields() []Field {
	return p.Fields
}

func (p *Plugin) GetName() string {
	return p.Name
}

func (p *Plugin) GetObjects() []Object {
	return p.Objects
}

func (p *Plugin) GetArrays() []Array {
	return p.Arrays
}

// InputsPlugin
type InputsPlugin struct {
	Plugin
}

func (p *InputsPlugin) GetType() string {
	return "inputs"
}

func NewInputsPlugin(name string, fields []Field) IPlugin {
	return &InputsPlugin{
		Plugin: Plugin{
			Name:   name,
			Fields: fields,
		},
	}
}

func NewInputsPluginWithTags(name string, fields []Field, tags []Tag) IPlugin {
	return &InputsPlugin{
		Plugin: Plugin{
			Name:   name,
			Fields: fields,
			Tags:   tags,
		},
	}
}

func NewInputsPluginWithObjects(name string, fields []Field, tags []Tag, objects []Object) IPlugin {
	return &InputsPlugin{
		Plugin: Plugin{
			Name:    name,
			Fields:  fields,
			Tags:    tags,
			Objects: objects,
		},
	}
}

// TODO: here we should add the redundant fileds to be included like: format_data, json_string_fields, etc...
func NewInputsPluginV2(plugin Plugin) IPlugin {
	return &InputsPlugin{
		Plugin: plugin,
	}
}

// OutputsPlugin
type OutputsPlugin struct {
	Plugin
}

func (p *OutputsPlugin) GetType() string {
	return "outputs"
}

func NewOutputsPlugin(name string, fields []Field) IPlugin {
	return &OutputsPlugin{
		Plugin: Plugin{
			Name:   name,
			Fields: fields,
		},
	}
}

// AggregatorsPlugin
type AggregatorsPlugin struct {
	Plugin
}

func (p *AggregatorsPlugin) GetType() string {
	return "aggregators"
}

func NewAggregatorsPlugin(name string, fields []Field) IPlugin {
	return &AggregatorsPlugin{
		Plugin: Plugin{
			Name:   name,
			Fields: fields,
		},
	}
}

func NewAggregatorsPluginWithTags(name string, fields []Field, tags []Tag) IPlugin {
	return &AggregatorsPlugin{
		Plugin: Plugin{
			Name:   name,
			Fields: fields,
			Tags:   tags,
		},
	}
}

// ProcessorsPlugin
type ProcessorsPlugin struct {
	Plugin
}

func (p *ProcessorsPlugin) GetType() string {
	return "processors"
}

func NewProcessorsPlugin(name string, fields []Field) IPlugin {
	return &ProcessorsPlugin{
		Plugin: Plugin{
			Name:   name,
			Fields: fields,
		},
	}
}

func NewProcessorsPluginWithTags(name string, fields []Field, tags []Tag) IPlugin {
	return &ProcessorsPlugin{
		Plugin: Plugin{
			Name:   name,
			Fields: fields,
			Tags:   tags,
		},
	}
}

func NewProcessorsPluginV2(plugin Plugin) IPlugin {
	return &ProcessorsPlugin{
		Plugin: plugin,
	}
}

// Configuration
type Configuration struct {
	Agent   *Agent
	Plugins []IPlugin
}
