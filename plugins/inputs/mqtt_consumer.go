// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package inputs

import (
	"bitbucket.org/averroes_ai/telegraf-client/entities"
)

type MQTTConsumerInputsPlugin struct {
	Servers          []string
	Topics           []string
	QOS              *int
	JsonStringFields []string
	DataFormat       string
	Tags             []entities.Tag
}

func NewMQTTConsumerInputsPlugin(plugin MQTTConsumerInputsPlugin) entities.IPlugin {
	qos := 1

	if plugin.QOS != nil {
		qos = *plugin.QOS
	}

	jsonStringFields := []string{"*"}

	if len(plugin.JsonStringFields) != 0 {
		jsonStringFields = plugin.JsonStringFields
	}

	dataFormat := "json"

	if plugin.DataFormat != "" {
		dataFormat = plugin.DataFormat
	}

	return entities.NewInputsPluginWithTags(
		"mqtt_consumer",
		[]entities.Field{
			{TomlKey: "servers", Value: plugin.Servers},
			{TomlKey: "topics", Value: plugin.Topics},
			{TomlKey: "qos", Value: qos},
			{TomlKey: "json_string_fields", Value: jsonStringFields},
			{TomlKey: "data_format", Value: dataFormat},
		},
		plugin.Tags,
	)
}
