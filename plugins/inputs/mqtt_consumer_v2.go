// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package inputs

import (
	"bitbucket.org/averroes_ai/telegraf-client/entities"
)

type MQTTConsumerV2InputsPlugin struct {
	Servers []string
	Topics  []string
	QOS     *int

	MachineId string
	DeviceId  string

	Tags []entities.Tag
}

func NewMQTTConsumerV2InputsPlugin(plugin MQTTConsumerV2InputsPlugin) entities.IPlugin {
	qos := 1

	if plugin.QOS != nil {
		qos = *plugin.QOS
	}

	return entities.NewInputsPluginV2(entities.Plugin{
		Name: "mqtt_consumer",
		Fields: []entities.Field{
			{TomlKey: "servers", Value: plugin.Servers},
			{TomlKey: "topics", Value: plugin.Topics},
			{TomlKey: "qos", Value: qos},
			{TomlKey: "json_string_fields", Value: []string{"*"}},
			{TomlKey: "data_format", Value: "json_v2"},
			{TomlKey: "alias", Value: ConstructInputPluginAlias(InputPluginAliasFields{
				MachineId: plugin.MachineId,
				DeviceId:  plugin.DeviceId,
			})},
		},
		Tags: plugin.Tags,
		Arrays: []entities.Array{
			{
				Name: "json_v2",
				Arrays: []entities.Array{
					{
						Name: "json_v2.object",
						Fields: []entities.Field{
							{TomlKey: "path", Value: "@this"},
						},
					},
				},
			},
		},
	})
}
