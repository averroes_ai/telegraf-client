// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package builder

import (
	"bitbucket.org/averroes_ai/telegraf-client/entities"
)

type ConfigurationBuilder struct {
	agent   *entities.Agent
	plugins []entities.IPlugin

	configuration string
}

func NewBuilder() *ConfigurationBuilder {
	return &ConfigurationBuilder{}
}

func (b *ConfigurationBuilder) Agent(agent entities.Agent) *ConfigurationBuilder {
	b.agent = &agent

	return b
}

func (b *ConfigurationBuilder) AddPlugin(plugin entities.IPlugin) *ConfigurationBuilder {
	b.plugins = append(b.plugins, plugin)

	return b
}

func (b *ConfigurationBuilder) Build() *entities.Configuration {
	return &entities.Configuration{
		Agent:   b.agent,
		Plugins: b.plugins,
	}
}
