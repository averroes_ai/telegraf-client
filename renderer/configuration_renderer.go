// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package renderer

import (
	"strings"
)

const (
	keyOpenDeterminator  = "{{"
	keyCloseDeterminator = "}}"
)

type Params map[string]string

func NewParams(params map[string]string) Params {
	return params
}

type ConfigurationRenderer struct {
	params  Params
	content string
}

func NewConfigurationRenderer() *ConfigurationRenderer {
	return &ConfigurationRenderer{}
}

func (r *ConfigurationRenderer) renderParams() string {
	newContent := r.content

	for key, value := range r.params {
		newContent = r.renderParam(newContent, key, value)
	}

	return newContent
}

func (r *ConfigurationRenderer) renderParam(content string, key string, value string) string {
	return strings.ReplaceAll(content, keyOpenDeterminator+key+keyCloseDeterminator, value)
}
