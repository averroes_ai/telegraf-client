// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package inputs

import (
	"bitbucket.org/averroes_ai/telegraf-client/entities"
)

type InfluxDBV2InputsPlugin struct {
	URL          string
	Organization string
	Token        string
	Query        string
	Timeout      *uint

	MachineId string
	DeviceId  string

	Tags []entities.Tag
}

func NewInfluxDBV2InputsPlugin(plugin InfluxDBV2InputsPlugin) entities.IPlugin {

	timeout := uint(20)

	if plugin.Timeout != nil {
		timeout = *plugin.Timeout
	}

	return entities.NewInputsPluginV2(entities.Plugin{
		Name: "influxdb_v2",
		Fields: []entities.Field{
			{TomlKey: "url", Value: plugin.URL},
			{TomlKey: "organization", Value: plugin.Organization},
			{TomlKey: "token", Value: plugin.Token},
			{TomlKey: "query", Value: plugin.Query},
			{TomlKey: "timeout", Value: timeout},
			{TomlKey: "json_string_fields", Value: []string{"*"}},
			{TomlKey: "data_format", Value: "json_v2"},
			{TomlKey: "alias", Value: ConstructInputPluginAlias(InputPluginAliasFields{
				MachineId: plugin.MachineId,
				DeviceId:  plugin.DeviceId,
			})},
		},
		Tags: plugin.Tags,
		Arrays: []entities.Array{
			{
				Name: "json_v2",
				Arrays: []entities.Array{
					{
						Name: "json_v2.object",
						Fields: []entities.Field{
							{TomlKey: "path", Value: "@this"},
						},
					},
				},
			},
		},
	})
}
