// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package encoder

import "bitbucket.org/averroes_ai/telegraf-client/entities"

type encoder func(entities.Configuration) string

var encoders = []encoder{marshalAgent, marshalPlugins}
