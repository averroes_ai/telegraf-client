// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package inputs

import (
	"bitbucket.org/averroes_ai/telegraf-client/entities"
)

type HTTPV2InputsPlugin struct {
	URLs     []string
	Method   string
	Username string
	Password string
	Headers  []entities.Field

	MachineId string
	DeviceId  string

	Tags []entities.Tag
}

func NewHTTPV2InputsPlugin(plugin HTTPV2InputsPlugin) entities.IPlugin {

	objects := []entities.Object{}

	if len(plugin.Headers) > 0 {
		objects = append(objects, entities.Object{
			Name:   "headers",
			Fields: plugin.Headers,
		})
	}

	return entities.NewInputsPluginV2(entities.Plugin{
		Name: "http",
		Fields: []entities.Field{
			{TomlKey: "urls", Value: plugin.URLs},
			{TomlKey: "method", Value: plugin.Method},
			{TomlKey: "username", Value: plugin.Username},
			{TomlKey: "password", Value: plugin.Password},
			{TomlKey: "json_string_fields", Value: []string{"*"}},
			{TomlKey: "data_format", Value: "json_v2"},
			{TomlKey: "alias", Value: ConstructInputPluginAlias(InputPluginAliasFields{
				MachineId: plugin.MachineId,
				DeviceId:  plugin.DeviceId,
			})},
		},
		Tags:    plugin.Tags,
		Objects: objects,
		Arrays: []entities.Array{
			{
				Name: "json_v2",
				Arrays: []entities.Array{
					{
						Name: "json_v2.object",
						Fields: []entities.Field{
							{TomlKey: "path", Value: "@this"},
						},
					},
				},
			},
		},
	})
}
