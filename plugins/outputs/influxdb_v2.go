// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package outputs

import "bitbucket.org/averroes_ai/telegraf-client/entities"

type InfluxDBV2OutputsPlugin struct {
	URLs         []string
	Token        string
	Organization string
	Bucket       string
	DataFormat   string
	Measurement  string
	ExcludedTags []string
}

func NewInfluxDBV2OutputsPlugin(plugin InfluxDBV2OutputsPlugin) entities.IPlugin {
	dataFormat := "json"

	if plugin.DataFormat != "" {
		dataFormat = plugin.DataFormat
	}

	excludedTags := []string{"machine_id"}

	if len(plugin.ExcludedTags) != 0 {
		excludedTags = plugin.ExcludedTags
	}

	return entities.NewOutputsPlugin(
		"influxdb_v2",
		[]entities.Field{
			{TomlKey: "urls", Value: plugin.URLs},
			{TomlKey: "token", Value: plugin.Token},
			{TomlKey: "organization", Value: plugin.Organization},
			{TomlKey: "bucket", Value: plugin.Bucket},
			{TomlKey: "data_format", Value: dataFormat},
			{TomlKey: "drop_string_fields", Value: true},
			{TomlKey: "name_override", Value: plugin.Measurement},
			{TomlKey: "tagexclude", Value: excludedTags},
		},
	)
}
