// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package renderer

import (
	"io/ioutil"
	"os"
)

func (r *ConfigurationRenderer) Render(configurationFilePath string, params Params) (*string, error) {
	r.params = params
	content, err := readFileContent(configurationFilePath)

	if err != nil {
		return nil, err
	}

	r.content = *content

	newContent := r.renderParams()

	return &newContent, nil
}

func readFileContent(filePath string) (*string, error) {
	file, err := os.Open(filePath)

	if err != nil {
		return nil, err
	}

	bytes, err := ioutil.ReadAll(file)

	if err != nil {
		return nil, err
	}

	content := string(bytes)

	file.Close()

	return &content, nil
}
