// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package encoder

import (
	"reflect"
	"strings"

	"bitbucket.org/averroes_ai/telegraf-client/entities"
)

const agentHeader = "[agent]"

func marshalAgent(configuration entities.Configuration) string {
	if configuration.Agent == nil {
		return ""
	}

	agentContent := agentHeader

	values := reflect.ValueOf(*configuration.Agent)

	for i := 0; i < values.Type().NumField(); i++ {
		field := values.Type().Field(i)
		value := values.Field(i)

		agentContent = strings.Join([]string{agentContent, marshalStructField(field, value)}, "\n")
	}

	return agentContent
}

func marshalStructField(field reflect.StructField, value reflect.Value) string {
	tag, ok := field.Tag.Lookup(tomlKey)

	if !ok {
		return ""
	}

	valueString := marshal(value.Interface())

	if valueString == "" || valueString == "\"\"" {
		return ""
	}

	return strings.Join(
		[]string{tag, "=", valueString},
		" ",
	)
}
