// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package client

import (
	"errors"
	"io"
	"os"
	"os/exec"
)

type TelegrafClient struct {
	tempDir      string
	telegrafPath string
}

func NewClientDefault(tempDir string) *TelegrafClient {
	return &TelegrafClient{
		tempDir:      tempDir,
		telegrafPath: "telegraf",
	}
}

func NewClient(telegrafPath string, tempDir string) *TelegrafClient {
	return &TelegrafClient{
		tempDir:      tempDir,
		telegrafPath: telegrafPath,
	}
}

type RunOutput struct {
	PID int
}

type RunWithConfigurationStringInput struct {
	Configuration string
	Stdout        io.Writer
	Stderr        io.Writer
}

func (c *TelegrafClient) RunWithConfigurationString(input RunWithConfigurationStringInput) (*RunOutput, error) {
	file, err := c.createFileForConfiguration(input.Configuration)

	if err != nil {
		return nil, err
	}

	output, err := c.RunWithConfigurationFile(RunWithConfigurationFileInput{
		ConfigurationFilePath: file.Name(),
		Stdout:                input.Stdout,
		Stderr:                input.Stderr,
	})

	file.Close()
	//os.Remove(file.Name())

	return output, err
}

func (c *TelegrafClient) createFileForConfiguration(configuration string) (*os.File, error) {
	file, err := os.CreateTemp(c.tempDir, "configuration-temp-file-*.conf")

	if err != nil {
		return nil, errors.New("error while creating configuration temp file: " + err.Error())
	}

	_, err = file.WriteString(configuration)

	if err != nil {
		return nil, errors.New("error while writing on configuration temp file: " + err.Error())
	}

	return file, nil
}

type RunWithConfigurationFileInput struct {
	ConfigurationFilePath string
	Stdout                io.Writer
	Stderr                io.Writer
}

func (c *TelegrafClient) RunWithConfigurationFile(input RunWithConfigurationFileInput) (*RunOutput, error) {
	command := c.telegrafPath + " --config " + input.ConfigurationFilePath
	cmd := exec.Command("bash", "-c", command)

	if input.Stdout != nil {
		cmd.Stdout = input.Stdout
	}

	if input.Stderr != nil {
		cmd.Stderr = input.Stderr
	}

	err := cmd.Start()

	if err != nil {
		return nil, errors.New("error while starting Telegraf command: " + err.Error())
	}

	return &RunOutput{
		PID: cmd.Process.Pid,
	}, nil
}
