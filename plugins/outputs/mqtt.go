// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package outputs

import (
	"bitbucket.org/averroes_ai/telegraf-client/entities"
)

type MQTTOutputsPlugin struct {
	Servers     []string
	TopicPrefix string
	Topic       string
	QOS         *int
	DataFormat  string
}

func NewMQTTOutputsPlugin(plugin MQTTOutputsPlugin) entities.IPlugin {
	qos := 1

	if plugin.QOS != nil {
		qos = *plugin.QOS
	}

	dataFormat := "json"

	if plugin.DataFormat != "" {
		dataFormat = plugin.DataFormat
	}

	return entities.NewOutputsPlugin(
		"mqtt",
		[]entities.Field{
			{TomlKey: "servers", Value: plugin.Servers},
			{TomlKey: "topic", Value: plugin.Topic},
			{TomlKey: "topic_prefix", Value: plugin.TopicPrefix},
			{TomlKey: "qos", Value: qos},
			{TomlKey: "data_format", Value: dataFormat},
		},
	)
}
