// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package inputs

import (
	"encoding/json"
	"log"
)

type InputPluginAliasFields struct {
	MachineId string `json:"machine_id"`
	DeviceId  string `json:"device_id"`
}

func ConstructInputPluginAlias(fields InputPluginAliasFields) string {
	value, err := json.Marshal(fields)

	if err != nil {
		log.Println("can't marshal input plugin [alias] fields,", fields)
		return ""
	}

	return string(value)
}
