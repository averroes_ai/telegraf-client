// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package main

import (
	"fmt"

	"bitbucket.org/averroes_ai/telegraf-client/builder"
	"bitbucket.org/averroes_ai/telegraf-client/encoder"
	"bitbucket.org/averroes_ai/telegraf-client/entities"
)

func main() {
	b := builder.NewBuilder()

	config := b.Agent(
		entities.Agent{
			Interval: "1s",
			Logfile:  "/abd/usr/local",
		},
	).
		AddPlugin(entities.NewInputsPluginV2(entities.Plugin{
			Name: "http",
			Fields: []entities.Field{
				{TomlKey: "urls", Value: []string{"http://safawi.greenpowermonitor.com:6601/api/Plant/2/Element/9497/LastData"}},
				{TomlKey: "method", Value: "GET"},
				{TomlKey: "username", Value: ""},
				{TomlKey: "password", Value: ""},
				{TomlKey: "json_string_fields", Value: []string{"*"}},
				{TomlKey: "data_format", Value: "json_v2"},
			},
			Tags: []entities.Tag{
				{Key: "machine_id", Value: "Wd2iB7HONcWPIiXiM"},
				{Key: "device_name", Value: "PPC"},
			},
			Objects: []entities.Object{
				{
					Name: "headers",
					Fields: []entities.Field{
						{TomlKey: "Authorization", Value: "Bearer AQAAANCMnd8BFdERjHoAwE_Cl-sBAAAA-Qcf2UmGJkKeOjjGUZuCXAAAAAACAAAAAAADZgAAwAAAABAAAAApVx-sP4R97CgmZrDGAUw4AAAAAASAAACgAAAAEAAAAFEj1T--PRVslYY1TzZ9vZjAAAAAT2B65c8JHutStyBcwyRxC1dN4db56CSa_aS-oigE8Sp9mok9VthR6UyEHwEGzDsPvd8BiThOqiprAwgW7L1BH8Ue6r-Uatu20IBmg_PPJbCDWlqShAYINMXBZqy4xRTiOCG2v5iPXxq5RtW79mxfHO6Li3zG2uwmlv1CFkNJwdxybAwpbTs3QZ5W-xQIhTaA-FWDusa8bA0-Jn-6ZcJsrDlcQ3-B-lE5OKYSuXvr-JWNNpqC_eBJ88ixi7-U0godFAAAAA-IJ2fG_j66e-udwXU_BT2K1k3M"},
					},
				},
			},
			Arrays: []entities.Array{
				{
					Name: "json_v2",
					Arrays: []entities.Array{
						{
							Name: "json_v2.object",
							Fields: []entities.Field{
								{TomlKey: "path", Value: "@this"},
							},
						},
					},
				},
			},
		})).
		AddPlugin(entities.NewInputsPluginV2(entities.Plugin{
			Name: "http",
			Fields: []entities.Field{
				{TomlKey: "urls", Value: []string{"http://safawi.greenpowermonitor.com:6601/api/Plant/2/Element/8824/LastData"}},
				{TomlKey: "method", Value: "GET"},
				{TomlKey: "username", Value: ""},
				{TomlKey: "password", Value: ""},
				{TomlKey: "json_string_fields", Value: []string{"*"}},
				{TomlKey: "data_format", Value: "json_v2"},
			},
			Tags: []entities.Tag{
				{Key: "machine_id", Value: "Wd2iB7HONcWPIiXiM"},
				{Key: "device_name", Value: "IS10-MS-01"},
			},
			Objects: []entities.Object{
				{
					Name: "headers",
					Fields: []entities.Field{
						{TomlKey: "Authorization", Value: "Bearer AQAAANCMnd8BFdERjHoAwE_Cl-sBAAAA-Qcf2UmGJkKeOjjGUZuCXAAAAAACAAAAAAADZgAAwAAAABAAAAApVx-sP4R97CgmZrDGAUw4AAAAAASAAACgAAAAEAAAAFEj1T--PRVslYY1TzZ9vZjAAAAAT2B65c8JHutStyBcwyRxC1dN4db56CSa_aS-oigE8Sp9mok9VthR6UyEHwEGzDsPvd8BiThOqiprAwgW7L1BH8Ue6r-Uatu20IBmg_PPJbCDWlqShAYINMXBZqy4xRTiOCG2v5iPXxq5RtW79mxfHO6Li3zG2uwmlv1CFkNJwdxybAwpbTs3QZ5W-xQIhTaA-FWDusa8bA0-Jn-6ZcJsrDlcQ3-B-lE5OKYSuXvr-JWNNpqC_eBJ88ixi7-U0godFAAAAA-IJ2fG_j66e-udwXU_BT2K1k3M"},
					},
				},
			},
			Arrays: []entities.Array{
				{
					Name: "json_v2",
					Arrays: []entities.Array{
						{
							Name: "json_v2.object",
							Fields: []entities.Field{
								{TomlKey: "path", Value: "@this"},
							},
						},
					},
				},
			},
		})).
		AddPlugin(entities.NewProcessorsPluginV2(entities.Plugin{
			Name: "mapper",
			Fields: []entities.Field{
				{TomlKey: "order", Value: 1},
				{TomlKey: "identifier_key", Value: "device_name"},
			},
			Arrays: []entities.Array{
				{
					Name: "settings",
					Fields: []entities.Field{
						{TomlKey: "type", Value: "list-of-objects"},
						{TomlKey: "identifier", Value: "PPC"},
						{TomlKey: "sensor_key_attribute", Value: "DataSourceId"},
						{TomlKey: "sensor_value_attribute", Value: "LastValue.Value"},
					},
					Objects: []entities.Object{
						{
							Name: "settings.dict_mapping_sensors",
							Fields: []entities.Field{
								{TomlKey: "236091", Value: "MAIN - ACTIVE POWER (kW)"},
								{TomlKey: "236092", Value: "MAIN - REACTIVE POWER (kVAr)"},
								{TomlKey: "236093", Value: "MAIN - GRID VOLTAGE V12 (V)"},
								{TomlKey: "236094", Value: "MAIN - GRID VOLTAGE V23 (V)"},
								{TomlKey: "236095", Value: "MAIN - GRID VOLTAGE V31 (V)"},
								{TomlKey: "236096", Value: "MAIN - GRID CURRENT I1 (A)"},
								{TomlKey: "236097", Value: "MAIN - GRID CURRENT I2 (A)"},
								{TomlKey: "236098", Value: "MAIN - GRID CURRENT I3 (A)"},
								{TomlKey: "236101", Value: "MAIN - GRID FREQUENCY (Hz)"},
							},
						},
					},
				},
				{
					Name: "settings",
					Fields: []entities.Field{
						{TomlKey: "type", Value: "list-of-objects"},
						{TomlKey: "identifier", Value: "IS10-MS-01"},
						{TomlKey: "sensor_key_attribute", Value: "DataSourceId"},
						{TomlKey: "sensor_value_attribute", Value: "LastValue.Value"},
					},
					Objects: []entities.Object{
						{
							Name: "settings.dict_mapping_sensors",
							Fields: []entities.Field{
								{TomlKey: "213918", Value: "WIND SPEED (m/s)"},
								{TomlKey: "213920", Value: "AIR TEMPERATURE (deg C)"},
								{TomlKey: "213921", Value: "RELATIVE HUMIDITY (%)"},
								{TomlKey: "213924", Value: "TILTED G.SOLAR R1 (W/m2)"},
							},
						},
					},
				},
			},
		})).
		Build()

	fmt.Println(encoder.Marshal(*config))
}
