// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package outputs

import "bitbucket.org/averroes_ai/telegraf-client/entities"

type KafkaProducerOutputsPlugin struct {
	Brokers         []string
	Topic           string
	DataFormat      string
	MetricBatchSize int
}

func NewKafkaProducerOutputsPlugin(plugin KafkaProducerOutputsPlugin) entities.IPlugin {
	dataFormat := "json"

	if plugin.DataFormat != "" {
		dataFormat = plugin.DataFormat
	}

	metricBatchSize := 1

	if plugin.MetricBatchSize != 0 {
		metricBatchSize = plugin.MetricBatchSize
	}

	return entities.NewOutputsPlugin(
		"kafka",
		[]entities.Field{
			{TomlKey: "brokers", Value: plugin.Brokers},
			{TomlKey: "topic", Value: plugin.Topic},
			{TomlKey: "data_format", Value: dataFormat},
			{TomlKey: "metric_batch_size", Value: metricBatchSize},
		},
	)
}
