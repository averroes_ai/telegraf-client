// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package processors

import "bitbucket.org/averroes_ai/telegraf-client/entities"

type PDMMachineProcessorsPlugin struct {
	Order   int
	Sensors []string
	Tags    []entities.Tag
}

func NewPDMMachineProcessorsPlugin(plugin PDMMachineProcessorsPlugin) entities.IPlugin {
	return entities.NewProcessorsPluginWithTags(
		"pdm_machine",
		[]entities.Field{
			{TomlKey: "order", Value: plugin.Order},
			{TomlKey: "sensors", Value: plugin.Sensors},
			{TomlKey: "json_string_fields", Value: []string{"*"}},
			{TomlKey: "data_format", Value: "json"},
		},
		plugin.Tags,
	)
}
