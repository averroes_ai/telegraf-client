// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package encoder

import (
	"bytes"
	"fmt"
	"strings"

	"bitbucket.org/averroes_ai/telegraf-client/entities"
)

const pluginHeader = "[[%v.%v]]"

const tagsHeader = "[%v.%v.tags]"

const objectHeader = "[%v.%v.%v]"

const arrayHeader = "[[%v.%v.%v]]"

func marshalPlugins(configuration entities.Configuration) string {
	var buffer bytes.Buffer

	for _, plugin := range configuration.Plugins {
		buffer.WriteString(marshalPlugin(plugin))
		buffer.WriteString("\n")
	}

	return buffer.String()
}

func marshalPlugin(plugin entities.IPlugin) string {

	var buffer bytes.Buffer

	header := fmt.Sprintf(pluginHeader, plugin.GetType(), plugin.GetName())

	buffer.WriteString(header + "\n")

	for _, field := range plugin.GetFields() {
		buffer.WriteString(marshalField(field))
		buffer.WriteString("\n")
	}

	if plugin.GetTags() != nil {
		buffer.WriteString(marshalTags(plugin))
	}

	if plugin.GetObjects() != nil {
		buffer.WriteString(marshalObjects(plugin))
	}

	if plugin.GetArrays() != nil {
		buffer.WriteString(marshalArrays(plugin))
	}

	return buffer.String()
}

func marshalTags(plugin entities.IPlugin) string {
	if len(plugin.GetTags()) == 0 {
		return ""
	}

	var buffer bytes.Buffer

	header := fmt.Sprintf(tagsHeader, plugin.GetType(), plugin.GetName())

	buffer.WriteString(header + "\n")

	for _, tag := range plugin.GetTags() {
		buffer.WriteString(marshalTag(tag))
		buffer.WriteString("\n")
	}

	return buffer.String()
}

func marshalField(field entities.Field) string {
	key := field.TomlKey

	if strings.Contains(field.TomlKey, " ") {
		key = strings.Join([]string{"\"", field.TomlKey, "\""}, "")
	}

	return strings.Join(
		[]string{
			key,
			"=",
			marshal(field.Value),
		},
		" ",
	)
}

func marshalTag(tag entities.Tag) string {
	return marshalField(entities.Field{TomlKey: tag.Key, Value: tag.Value})
}

func marshalObjects(plugin entities.IPlugin) string {
	if len(plugin.GetObjects()) == 0 {
		return ""
	}

	var buffer bytes.Buffer

	for _, object := range plugin.GetObjects() {
		buffer.WriteString(marshalObject(object, plugin))
		buffer.WriteString("\n")
	}

	return buffer.String()
}

func marshalObject(object entities.Object, plugin entities.IPlugin) string {

	var buffer bytes.Buffer

	header := fmt.Sprintf(objectHeader, plugin.GetType(), plugin.GetName(), object.Name)

	buffer.WriteString(header + "\n")

	for _, field := range object.Fields {
		buffer.WriteString(marshalField(field))
		buffer.WriteString("\n")
	}

	return buffer.String()
}

func marshalArrays(plugin entities.IPlugin) string {
	if len(plugin.GetArrays()) == 0 {
		return ""
	}

	var buffer bytes.Buffer

	for _, array := range plugin.GetArrays() {
		buffer.WriteString(marshalArray(array, plugin))
		buffer.WriteString("\n")
	}

	return buffer.String()
}

func marshalArray(array entities.Array, plugin entities.IPlugin) string {

	var buffer bytes.Buffer

	header := fmt.Sprintf(arrayHeader, plugin.GetType(), plugin.GetName(), array.Name)

	buffer.WriteString(header + "\n")

	for _, field := range array.Fields {
		buffer.WriteString(marshalField(field))
		buffer.WriteString("\n")
	}

	for _, object := range array.Objects {
		buffer.WriteString(marshalObject(object, plugin))
		buffer.WriteString("\n")
	}

	for _, a := range array.Arrays {
		buffer.WriteString(marshalArray(a, plugin))
		buffer.WriteString("\n")
	}

	return buffer.String()
}
