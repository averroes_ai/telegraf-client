// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package aggregators

import "bitbucket.org/averroes_ai/telegraf-client/entities"

type PDMMachineAggregatorsPlugin struct {
	Sensors []string
	Period  string
	Tags    []entities.Tag
}

func NewPDMMachineAggregatorsPlugin(plugin PDMMachineAggregatorsPlugin) entities.IPlugin {
	period := "1s"

	if plugin.Period != "" {
		period = plugin.Period
	}

	return entities.NewAggregatorsPluginWithTags(
		"pdm_machine",
		[]entities.Field{
			{TomlKey: "sensors", Value: plugin.Sensors},
			{TomlKey: "drop_original", Value: true},
			{TomlKey: "period", Value: period},
			{TomlKey: "json_string_fields", Value: []string{"*"}},
			{TomlKey: "data_format", Value: "json"},
		},
		plugin.Tags,
	)
}
