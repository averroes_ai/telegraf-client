// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package encoder

import (
	"bytes"
	"encoding/json"

	"bitbucket.org/averroes_ai/telegraf-client/entities"
)

const tomlKey = "toml"

func Marshal(configuration entities.Configuration) string {

	var buffer bytes.Buffer

	for _, e := range encoders {
		buffer.WriteString(e(configuration))
		buffer.WriteString("\n")
	}

	return buffer.String()
}

func marshal(value interface{}) string {
	marshaledValue, _ := json.Marshal(value)

	return string(marshaledValue)
}
