// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package renderer

func (r *ConfigurationRenderer) RenderString(configurationString string, params Params) (*string, error) {
	r.params = params

	r.content = configurationString

	newContent := r.renderParams()

	return &newContent, nil
}
