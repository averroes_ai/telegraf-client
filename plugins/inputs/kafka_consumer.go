// ------------------------------------------------------------------------------
// Copyright (c) 2024 Averroes.ai Inc.
// All rights reserved.
//
// This software and associated documentation files (the "Software") may not be
// copied, modified, distributed, or used without express permission from
// Averroes.ai Inc.
//
// For inquiries, contact:
// Averroes.ai Inc.
// Legal@averroes.ai
// ------------------------------------------------------------------------------

package inputs

import "bitbucket.org/averroes_ai/telegraf-client/entities"

type KafkaConsumerInputsPlugin struct {
	Brokers    []string
	Topics     []string
	DataFormat string
	Tags       []entities.Tag
}

func NewKafkaConsumerInputsPlugin(plugin KafkaConsumerInputsPlugin) entities.IPlugin {
	dataFormat := "json"

	if plugin.DataFormat != "" {
		dataFormat = plugin.DataFormat
	}

	return entities.NewInputsPluginWithTags(
		"kafka_consumer",
		[]entities.Field{
			{TomlKey: "brokers", Value: plugin.Brokers},
			{TomlKey: "topics", Value: plugin.Topics},
			{TomlKey: "data_format", Value: dataFormat},
		},
		plugin.Tags,
	)
}
